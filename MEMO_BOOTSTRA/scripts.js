$(function () {
    video();
    // CREATE/POST
    $('#button_add').on('click', function (event) {
        var d = new Date();
        event.preventDefault();
        var createInput = $('#create-input');
        var createTitle = $('#create-title');
        console.log(createTitle.val());
        $.ajax({
            url: '/aggiungi',
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ name: createInput.val(), title: createTitle.val(),data:d.toDateString() }),
            success: function (response) {
                console.log(response);
                createInput.val('');
                createTitle.val('');
                video();
            },
            error: function () {
                alert("AGGIUNTA FALLITA")
            },
        });
    });

});
function video() {
    $.ajax({
        url: '/memo',
        contentType: 'application/json',
        success: function (response) {
            var tbodyEl = $('#dinamico');
            tbodyEl.html('');
            response.forEach(function (product) {
                tbodyEl.append('<article><div class="memo" id="mem' + product.id + '" style="margin-top:25px"><input type="text" class="id" value="' + product.id + '"><div id="testo"class="card text-white bg-warning mb-3" style="max-width: 18rem;"> <div class="card-header">' + product.data + '<div class="float-right btn-group btn-group-sm"><a class="btn btn-warning tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Edit" onclick="aggiorna(' + product.id + ')"><i class="fa fa-pencil"></i> </a><a class="btn btn-warning tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Delete" onclick="elimina(' + product.id + ')"><i class="fa fa-times"></i></a></div></div><div class="card-body"><h2 class="card-title">' + product.title + '</h2><p class="card-text">' + product.name + '</p> </div></div></div></article>');
            });
        },
        error: function () {
            alert("CHIAMATa FALLITA")
        },
    });
};

// DELETE
function elimina(id) {
    $.ajax({
        url: '/elimina/' + id,
        method: 'DELETE',
        contentType: 'application/json',
        success: function (response) {
            console.log(response);
            video();
        },
        error: function () {
            alert("CANCELLAZIONE FALLITA")
        },
    });
};

///update
function aggiorna(id){
    console.log("id" + id);
    var createInput = $('#create-input').val();
    var createTitle = $('#create-title').val();
    $.ajax({
        url: '/aggiorna/' + id,
        method: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify({ newName: createInput, newTitle: createTitle }),
        success: function (response) {
            console.log(response);
            video();
        },
        error: function () {
            alert("AGGIORNAMENTO FALLITO")
        },
    })
}
