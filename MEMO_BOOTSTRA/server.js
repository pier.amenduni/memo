var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const Sequelize = require('sequelize');


// ***** DB *****

// db connection
let sequelize = new Sequelize('sequelize', 'sequelize', 'sequelize@2019', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});
// sync models
const Memo = sequelize.import(__dirname + '/memo');
Memo.sync();
// ***** END DB *****

// **** REST ******

app.use(express.static(__dirname));
app.use(express.json());

app.get('/memo', function (req, res) {
    Memo.findAll().then(memos => {
        res.json(memos);
    })
});

app.post('/aggiungi', function (req, res) {
    var Name = req.body.name;
    var Titolo = req.body.title;
    var Data=req.body.data;
    Memo.create({ name: Name, title: Titolo,data:Data }).then(memo => {
        res.json({ msg: `memo: ${memo} created` });
    })
});

app.put('/aggiorna/:id', function (req, res) {
    var id = req.params.id;
    var newName = req.body.newName;
    var newTitle=req.body.newTitle;

    Memo.update({
        title:newTitle,
        name:newName
    },
        {
            where: {
                id: id
            }
        }).then(memo=>{
            console.log("AGGIORNATO");
            res.json({ msg: `memo: ${memo} created` });
         })
});

app.delete('/elimina/:id', function (req, res) {
    var id = req.params.id;
    var found;
    Memo.destroy({
        where: {
            id: id
        }
    }).then(
        res.send('Successfully deleted product!')
    )
});

app.listen(3000, function () {
    console.log('Server listening on ' + 3000);
});
